import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom'; //para probar el route
import logo from '../tortilla.png';
import './App.css';
import { HistorialPedidos } from '../HistorialPedidos'; //Agrege la ruta para ver mis cambios
import { ListaPedido, ListaVenta } from '../Lista';
import { ItemPedido, ItemVenta } from '../Item';
import { Link } from "react-router-dom";

import Boton from '../Boton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOut, faHistory } from '@fortawesome/free-solid-svg-icons'

//este import permite utilizar el elemento botón
//import Boton from '../Boton';

// Imports de Font Awesome
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { faBook, faCheckSquare, faCreditCard, faDollarSign, faHistory, faList, faMoneyBill, faPen, faRemove, faSearch, faSignIn, faSignOut, faUser } from '@fortawesome/free-solid-svg-icons'


function App() {

  /*
  //Arreglos de datos Lista
  //Array inicial de pedidos
  const pedidosArray = [
    {
      numeropedido: 1,
      fecha: '10-12-22',
      hora: '11:15',
      status: false,
    },
    {
      numeropedido: 2,
      fecha: '11-12-22',
      hora: '11:16',
      status: true,
    },
    {
      numeropedido: 3,
      fecha: '12-12-22',
      hora: '11:17',
      status: false,
    },
    {
      numeropedido: 4,
      fecha: '13-12-22',
      hora: '11:18',
      status: false,
    },
  ]

  const ventasArray = [
    {
      numerodeventa: 1,
      fecha: '10-12-22',
      monto: 100,
    },
    {
      numerodeventa: 2,
      fecha: '11-12-22',
      monto: 200,
    },
    {
      numerodeventa: 3,
      fecha: '12-12-22',
      monto: 300,
    },
    {
      numerodeventa: 4,
      fecha: '14-12-22',
      monto: 400,
    },
  ]

  //States Lista
  const [pedidos, setPedidos] = React.useState(pedidosArray);
  const [ventas, setVentas] = React.useState(ventasArray);

  //Funciones Lista 
  //Aceptar pedido
  const onAccept = (numeropedido) => {
    const index = pedidos.findIndex(pedido => pedido.numeropedido === numeropedido);
    const newPedidos = [...pedidos];
    newPedidos[index].status = true;
    setPedidos(newPedidos);
  }
  */

  return (
    <BrowserRouter>
      <Routes>
      <Route path = '/' element={
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/*Iconos de Font Awesome*/}
        {/*<FontAwesomeIcon icon={faDollarSign} />
        <FontAwesomeIcon icon={faPen} />
        <FontAwesomeIcon icon={faMoneyBill} />
        <FontAwesomeIcon icon={faCreditCard} />
        <FontAwesomeIcon icon={faSearch} />
        <FontAwesomeIcon icon={faHistory} />
        <FontAwesomeIcon icon={faBook} />
        <FontAwesomeIcon icon={faList} />
        <FontAwesomeIcon icon={faSignIn} />
        <FontAwesomeIcon icon={faSignOut} />
        <FontAwesomeIcon icon={faCheckSquare} />
        <FontAwesomeIcon icon={faRemove} />
        <FontAwesomeIcon icon={faUser} /> 
    */}
                        
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {/* este es un ejemplo de cómo se implementa el elemento botón, 
          en color se deben usar las clases que provee boostrap por ejemplo: 
          danger, primary success... */
          //<Boton text='Este es el texto del botón' color='danger'/>
          } 

          Learn React
        </a>
        <Link to={`/HistorialPedidos`} className="btn btn-success">
              <FontAwesomeIcon
                size="1x"
                icon={faSignOut}
        /></Link>     
      </header>
      
      {/* Uso de Componente Lista e Item 
      //Tabla ventas 
      <ListaVenta>
        {ventas.map(venta => (
          <ItemVenta
            primero={venta.numerodeventa}
            segundo={venta.fecha}
            tercero={venta.monto}
          />
        ))
        }
      </ListaVenta>
      //Tabla pedidos 
      <ListaPedido>
        {pedidos.map(pedido => (
          <ItemPedido
            primero={pedido.numeropedido}
            segundo={pedido.fecha}//No es necesario mandarlo en la lista de ventas
            tercero={pedido.hora}
            cuarto={pedido.status}
            onAccept={() => onAccept(pedido.numeropedido)}
          />
        ))
        }
      </ListaPedido>
      */}
    </div>
    } />
    {/*Ruta al historial */}
    <Route path = '/HistorialPedidos' element={
          <HistorialPedidos 
          
          />
        } />
    </Routes>
  </BrowserRouter> //prueba de route ruta raiz
  );
}

export default App;
